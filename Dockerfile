FROM openjdk:11
ARG JAR_FILE=build/libs/BackEnd-Third-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} BackEnd-Third-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/BackEnd-Third-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
