package com.web.common.config;

        import com.querydsl.jpa.impl.JPAQueryFactory;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;
        import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
        import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
        import org.springframework.web.servlet.config.annotation.CorsRegistry;
        import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

        import javax.persistence.EntityManager;
        import javax.persistence.PersistenceContext;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry)
    {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:8081");

        registry.addMapping("/**")
                .allowedOrigins("http://61.77.247.76:8080");
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:8080");

        registry.addMapping("/**")
                .allowedOrigins("http://61.77.247.76:6060").allowedMethods("GET", "POST","PUT", "DELETE");
        registry.addMapping("/**")
                .allowedOrigins("*").allowedMethods("GET", "POST","PUT", "DELETE");
    }
}
