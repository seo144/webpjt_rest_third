package com.web.common.exception;

import lombok.Getter;

@Getter
public class CommonException extends RuntimeException {
    private final CommonExceptionCode exceptionCode;

    public CommonException(CommonExceptionCode code){
        super(code.getMessage());
        this.exceptionCode=code;
    }

}
