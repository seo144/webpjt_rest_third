package com.web.common.exception;


public interface CommonExceptionCode {
    int getStatus();
    String getErrorCode();
    String getMessage();

}
