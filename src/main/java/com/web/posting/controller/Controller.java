package com.web.posting.controller;


import com.web.posting.dto.MessageDto;
import com.web.posting.dto.RoomInfosDto;
import com.web.posting.dto.TagsDto;
import com.web.posting.entity.PostInfoDTO;
import com.web.privatePosting.dto.RoomIdDTO;
import com.web.posting.service.PostContentService;
import com.web.posting.service.PostInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class Controller {

    private final PostContentService postService;

    private final PostInfoService postInfoService;


    @PostMapping(value = "/v1/sendMessage")
    public String sendMessage(@RequestBody MessageDto messageDto){
        return postService.saveContent(messageDto);
    }

    @GetMapping(value = "/v1/TitleByTag")
    public RoomInfosDto getTitleByTag(@RequestBody TagsDto tagsDto)
    {
        return postService.getRoomInfos(tagsDto);

    }

    @GetMapping(value="/v1/allPostInfo")
    public RoomInfosDto getAllPostInfo()
    {
        return postService.getAllRoomInfos();
    }

    @GetMapping(value = "/v1/contentByRoomId/{roomId}")
    public MessageDto getcontentByRoomId(@PathVariable String roomId)
    {
        return postService.getPostInfosByRoomId(RoomIdDTO.builder().roomId(roomId).build());
    }

    @GetMapping(value ="/v1/allPostList")
    public List<PostInfoDTO> allPostInfo(){
        return postInfoService.findAllPostInfo();

    }

}
