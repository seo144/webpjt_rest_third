package com.web.posting.controller;

import com.web.posting.dto.PostCommentDto;
import com.web.posting.service.PostCommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/comments")
public class PostCommentController {
    private final PostCommentService postCommentService;

    @PostMapping
    public PostCommentDto setComment(@RequestBody PostCommentDto postCommentDto){
        return postCommentService.setComment(postCommentDto);
    }

    @GetMapping
    public List<PostCommentDto> getCommentsByContent(@RequestParam UUID contentUuid){
        return postCommentService.getComments(contentUuid);
    }
}
