package com.web.posting.dto;


import com.web.posting.vo.ChatMessageVO;
import com.web.posting.vo.CodeMessageVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageDto {
    private RoomInfoDto roomInfo;
    private String answer;
    private String question;
    private ArrayList<ChatMessageVO> chatMessageList;
    private ArrayList<CodeMessageVO> codeMessageList;
}
