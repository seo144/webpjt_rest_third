package com.web.posting.dto;

import com.web.posting.entity.PostCommentEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostCommentDto {
    private UUID commentUuid;
    private String userName;
    private UUID parentUuid;
    private String comments;
    private LocalDateTime modDate;

    public PostCommentEntity toEntity(){
        return PostCommentEntity.builder()
                .commentUuid(commentUuid)
                .userName(userName)
                .parentUuid(parentUuid)
                .comments(comments)
                .modDate(modDate)
                .build();
    }

    public static PostCommentDto of(PostCommentEntity entity){
        return PostCommentDto.builder()
                .commentUuid(entity.getCommentUuid())
                .userName(entity.getUserName())
                .comments(entity.getComments())
                .parentUuid(entity.getParentUuid())
                .modDate(entity.getModDate())
                .build();
    }
}
