package com.web.posting.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoomInfosDto {
    private ArrayList<RoomInfoDto> roomInfos = new ArrayList<>();

    public void add(RoomInfoDto roomInfoDto){
        roomInfos.add(roomInfoDto);
    }
}
