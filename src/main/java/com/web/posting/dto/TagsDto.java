package com.web.posting.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TagsDto {
    ArrayList<String> tags;

    public int size(){
        return tags.size();
    }
}
