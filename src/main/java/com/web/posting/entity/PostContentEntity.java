package com.web.posting.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="POST_CONTENT")
public class PostContentEntity
{

    @EmbeddedId
    private PostContentKeyEntity key;

    @Column
    private String userName;

    @Column
    private String content;
    
    @Column
    private LocalDateTime time;
    
    @Column
    private String mark;

    @Column
    private LocalDateTime makeTime;


}
