package com.web.posting.entity;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class PostContentKeyEntity  implements Serializable {

    @Column
    private String joinRoom;
    @Column
    private int orderby;


}
