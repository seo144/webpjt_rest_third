package com.web.posting.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Post_Info")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class PostInfoEntity {
    @Id
    @Column
    private String roomId;

    @Column
    private String title;

    @Column
    private String question;

    @Column String answer;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "roomId")
    List<RoomInfoEntity> roomInfos = new ArrayList<>();
}
