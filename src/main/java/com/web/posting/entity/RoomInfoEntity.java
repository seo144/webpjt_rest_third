package com.web.posting.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "ROOM_INFO")
@IdClass(RoomInfoKeyEntity.class)
public class RoomInfoEntity {

    //@EmbeddedId
    //private RoomInfoKeyEntity key;
    @Column
    @Id
    private String roomId;

    @Column
    @Id
    private String tag;

    @Column
    private String title;


}
