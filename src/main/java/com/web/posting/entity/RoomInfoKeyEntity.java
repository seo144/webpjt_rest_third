package com.web.posting.entity;

import lombok.*;

import javax.persistence.Column;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoomInfoKeyEntity implements Serializable {

    @Column
    private String roomId;
    @Column
    private String tag;

}
