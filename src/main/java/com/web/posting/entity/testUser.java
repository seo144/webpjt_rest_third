package com.web.posting.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@NoArgsConstructor
@Entity
public class testUser {

    @Id
    private String id;

    @Column(length = 500, nullable = false)
    private String title;

    @Column(length=5000, nullable = false)
    private String content;

    @Builder
    public testUser(String id, String title, String content){
        this.id = id;
        this.title = title;
        this.content = content;
    }

}
