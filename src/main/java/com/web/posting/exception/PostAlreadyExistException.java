package com.web.posting.exception;

import com.web.common.exception.CommonException;

public class PostAlreadyExistException extends CommonException {
    public PostAlreadyExistException(){
        super(PostringExceptionCode.POST_ALREADY_EXIST);
    }
}
