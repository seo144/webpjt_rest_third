package com.web.posting.exception;

import com.web.common.exception.CommonExceptionCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PostringExceptionCode implements CommonExceptionCode {
    POST_ALREADY_EXIST(400,"POST-1","이미 저장된 Postinn입니다."),
    ;
    private final int status;
    private final String errorCode;
    private final String message;
}
