package com.web.posting.repsitories;

import com.web.posting.entity.PostCommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PostCommentRepository extends JpaRepository<PostCommentEntity, UUID>, PostCommentRepositoryCustom{
}
