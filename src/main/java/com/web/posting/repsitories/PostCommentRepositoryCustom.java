package com.web.posting.repsitories;

import com.web.posting.entity.PostCommentEntity;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

public interface PostCommentRepositoryCustom {
    List<PostCommentEntity> findByContentUuid(UUID contentUuid);
}
