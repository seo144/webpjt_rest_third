package com.web.posting.repsitories;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.web.posting.entity.PostCommentEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

import static com.web.posting.entity.QPostCommentEntity.postCommentEntity;

@Repository
@RequiredArgsConstructor
public class PostCommentRepositoryImpl implements PostCommentRepositoryCustom {
    private final JPAQueryFactory queryFactory;
    public List<PostCommentEntity> findByContentUuid(UUID contentUuid){
        return queryFactory.selectFrom(postCommentEntity)
                .where(
                        postCommentEntity.parentUuid.eq(contentUuid)
                )
                .orderBy(postCommentEntity.modDate.asc())
                .fetch();
    }
}
