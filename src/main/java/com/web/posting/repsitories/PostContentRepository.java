package com.web.posting.repsitories;

import com.web.posting.entity.PostContentEntity;
import com.web.posting.entity.PostContentKeyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PostContentRepository extends JpaRepository<PostContentEntity, PostContentKeyEntity> {

    @Query(value = "SELECT * FROM POST_CONTENT WHERE JOIN_ROOM = ?1 ORDER BY ORDERBY ASC",nativeQuery = true)
    List<PostContentEntity> findByRoomId(String roomId);
}
