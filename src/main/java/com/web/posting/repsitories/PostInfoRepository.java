package com.web.posting.repsitories;

import com.web.posting.entity.PostInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostInfoRepository extends JpaRepository<PostInfoEntity,String> {
    List<PostInfoEntity> findByRoomId(String roomId);
    List<PostInfoEntity> findByRoomInfosTag(String tag);

}
