package com.web.posting.repsitories;

import com.web.posting.entity.RoomInfoEntity;
import com.web.posting.entity.RoomInfoKeyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

public interface RoomInfoRepository extends JpaRepository<RoomInfoEntity, RoomInfoKeyEntity> {

    //@Query(value = "SELECT * FROM ROOM_INFO WHERE tag = ?1",nativeQuery = true)
    //List<RoomInfoEntity> findByIdTag(String tag);

    List<RoomInfoEntity> findByTag(String tag);

    ArrayList<RoomInfoEntity> findByRoomId(String roomId);
}
