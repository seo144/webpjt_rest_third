package com.web.posting.service;

import com.web.posting.dto.PostCommentDto;
import com.web.posting.repsitories.PostCommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostCommentService {
    private final PostCommentRepository postCommentRepository;
    public PostCommentDto setComment(PostCommentDto postCommentDto){
        return PostCommentDto.of(postCommentRepository.save(postCommentDto.toEntity()));
    }


    public List<PostCommentDto> getComments(UUID contentUuid){
        return postCommentRepository.findByContentUuid(contentUuid).stream().map(PostCommentDto::of).collect(Collectors.toUnmodifiableList());
    }
}
