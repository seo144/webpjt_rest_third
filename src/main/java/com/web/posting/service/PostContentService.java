package com.web.posting.service;

import com.web.posting.dto.MessageDto;
import com.web.posting.dto.RoomInfoDto;
import com.web.posting.dto.RoomInfosDto;
import com.web.posting.dto.TagsDto;
import com.web.posting.exception.PostAlreadyExistException;
import com.web.posting.repsitories.PostContentRepository;
import com.web.posting.repsitories.PostInfoRepository;
import com.web.posting.repsitories.RoomInfoRepository;
import com.web.privatePosting.dto.RoomIdDTO;
import com.web.posting.entity.*;
import com.web.posting.vo.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PostContentService {
    private final PostContentRepository postContentRep;
    private final RoomInfoRepository roomInfoRep;
    private final PostInfoRepository postInfoRepository;

    public String saveContent(MessageDto messageDto){
        System.out.println("all saveCon");
        String roomId = messageDto.getRoomInfo().getRoomId();
        String title = messageDto.getRoomInfo().getTitle();
        for(int i = 0; i< messageDto.getRoomInfo().getTags().size(); i++)
        {
            RoomInfoEntity param = RoomInfoEntity.builder()
                    .roomId(messageDto.getRoomInfo().getRoomId())
                    .tag(messageDto.getRoomInfo().getTags().get(i))
                    .title(messageDto.getRoomInfo().getTitle())
                    .build();
            roomInfoRep.save(param);
        }

        PostInfoEntity postInfoEntity = PostInfoEntity.builder()
                .roomId(roomId)
                .title(title)
                //.RoomInfos(roomInfos)
                .answer(messageDto.getAnswer())
                .question(messageDto.getQuestion())
                .build();
        postInfoRepository.save(postInfoEntity);

        // messageVo 내 chat과 code끼리 시간이 빠른 순으로 postContent에 저장
        int chatSize = messageDto.getChatMessageList().size();
        int codeSize = messageDto.getCodeMessageList().size();
        // chat과 code 첫번째 항목 찾기
        int chatCnt=0;
        while(chatCnt < chatSize)
        {
            if (messageDto.getChatMessageList().get(chatCnt).getContent() != null)
                break;
            chatCnt++;
        }

        int codeCnt=0;
        while(codeCnt < codeSize)
        {
            if (messageDto.getCodeMessageList().get(codeCnt).getContent() != null)
                break;
            codeCnt++;
        }
        System.out.println("chatCnt : "+chatCnt);
        System.out.println("codeCnt : "+codeCnt);


        int order =0;
        LocalDateTime mkTime = LocalDateTime.now();
        PostContentKeyEntity CheckKey = PostContentKeyEntity.builder()
                .joinRoom(roomId)
                .orderby(0)
                .build();
        Optional<PostContentEntity> res =  postContentRep.findById(CheckKey);
        postExistValidation(res);

        // chatMessage나 codeMessage 둘중 하나라도 끝까지 갔으면 종료
        while(chatCnt < chatSize && codeCnt < codeSize)
        {
            // code Message가 빠를 경우
            if(messageDto.getCodeMessageList().get(codeCnt).getTime().isBefore( messageDto.getChatMessageList().get(chatCnt).getTime()))
            {
                // code message content가 null이 아닐 경우
                if (messageDto.getCodeMessageList().get(codeCnt).getContent() != null) {
                    PostContentKeyEntity key = PostContentKeyEntity.builder()
                            .orderby(order++)
                            .joinRoom(roomId)
                            .build();
                    PostContentEntity param = PostContentEntity.builder()
                            .content(messageDto.getCodeMessageList().get(codeCnt).getContent())
                            .mark(messageDto.getCodeMessageList().get(codeCnt).getMark())
                            .time(messageDto.getCodeMessageList().get(codeCnt).getTime())
                            .userName(messageDto.getCodeMessageList().get(codeCnt).getUserName())
                            .key(key)
                            .makeTime(mkTime)
                            .build();
                    postContentRep.save(param);
                }
                codeCnt++;
            }
            else    // chat Message 가 빠를 경우
            {
                // chat Message Content가 null이 아닐 경우
                if(messageDto.getChatMessageList().get(chatCnt).getContent() != null)
                {
                    PostContentKeyEntity key = PostContentKeyEntity.builder()
                            .orderby(order++)
                            .joinRoom(roomId)
                            .build();
                    PostContentEntity param = PostContentEntity.builder()
                            .content(messageDto.getChatMessageList().get(chatCnt).getContent())
                            .time(messageDto.getChatMessageList().get(chatCnt).getTime())
                            .userName(messageDto.getChatMessageList().get(chatCnt).getUserName())
                            .key(key)
                            .makeTime(mkTime)
                            .build();
                    postContentRep.save(param);
                }
                chatCnt++;
            }


        }

        if (chatCnt== chatSize)
        {
            for(int i =codeCnt;i<codeSize;i++)
            {
                if (messageDto.getCodeMessageList().get(i).getContent() != null) {
                    PostContentKeyEntity key = PostContentKeyEntity.builder()
                            .orderby(order++)
                            .joinRoom(roomId)
                            .build();
                    PostContentEntity param = PostContentEntity.builder()
                            .content(messageDto.getCodeMessageList().get(i).getContent())
                            .mark(messageDto.getCodeMessageList().get(i).getMark())
                            .time(messageDto.getCodeMessageList().get(i).getTime())
                            .userName(messageDto.getCodeMessageList().get(i).getUserName())
                            .key(key)
                            .makeTime(mkTime)
                            .build();
                    postContentRep.save(param);
                }
            }
        }else{
            for(int i =chatCnt;i<chatSize;i++)
            {
                if(messageDto.getChatMessageList().get(i).getContent() != null)
                {
                    PostContentKeyEntity key = PostContentKeyEntity.builder()
                            .orderby(order++)
                            .joinRoom(roomId)
                            .build();
                    PostContentEntity param = PostContentEntity.builder()
                            .content(messageDto.getChatMessageList().get(i).getContent())
                            .time(messageDto.getChatMessageList().get(i).getTime())
                            .userName(messageDto.getChatMessageList().get(i).getUserName())
                            .key(key)
                            .makeTime(mkTime)
                            .build();
                    postContentRep.save(param);
                }
            }
        }
        return "성공";
    }

    public RoomInfosDto getRoomInfos(TagsDto tagsDto) {
        RoomInfosDto roomInfosDto = new RoomInfosDto();
        for(int i = 0; i< tagsDto.size(); i++) {

            List<RoomInfoEntity> res = roomInfoRep.findByTag(tagsDto.getTags().get(i));
            for (int j = 0; j < res.size(); j++) {
                ArrayList<String> tag = new ArrayList<>();
                tag.add(res.get(j).getTag());
                RoomInfoDto roomInfoDto = RoomInfoDto.builder()
                                .roomId(res.get(j).getRoomId())
                                .title(res.get(j).getTitle())
                                .tags(tag)
                                .build();
                roomInfosDto.add(roomInfoDto);
            }
        }
        return roomInfosDto;
    }

    public RoomInfosDto getAllRoomInfos()
    {
        RoomInfosDto roomInfosDto = new RoomInfosDto();
        List<RoomInfoEntity> res = roomInfoRep.findAll();
        for (int j = 0; j < res.size(); j++) {
            ArrayList<String> tag = new ArrayList<>();
            tag.add(res.get(j).getTag());
            RoomInfoDto roomInfoDto = RoomInfoDto.builder()
                    .roomId(res.get(j).getRoomId())
                    .title(res.get(j).getTitle())
                    .tags(tag)
                    .build();
            roomInfosDto.add(roomInfoDto);
        }
        return roomInfosDto;
    }

    public MessageDto getPostInfosByRoomId(RoomIdDTO roomIdDTO){
        List<PostContentEntity> res = postContentRep.findByRoomId(roomIdDTO.getRoomId());

        ArrayList<ChatMessageVO> chatList = new ArrayList<>();
        ArrayList<CodeMessageVO> codeList = new ArrayList<>();

        for(int i =0;i<res.size();i++)
        {
            if (res.get(i).getMark() == null)
            {
                ChatMessageVO chat = ChatMessageVO.builder()
                            .joinRoom(res.get(i).getKey().getJoinRoom())
                            .content(res.get(i).getContent())
                            .time(res.get(i).getMakeTime())
                            .userName(res.get(i).getUserName())
                            .msgDate(res.get(i).getTime().toString())
                            .build();
                chatList.add(chat);
            }
            else
            {
                CodeMessageVO code = CodeMessageVO.builder()
                        .content(res.get(i).getContent())
                        .time(res.get(i).getMakeTime())
                        .mark(res.get(i).getMark())
                        .userName(res.get(i).getUserName())
                        .msgDate(res.get(i).getTime().toString())
                        .build();
                codeList.add(code);
            }
        }

        List<PostInfoEntity> room = postInfoRepository.findByRoomId(roomIdDTO.getRoomId());

        // 제약 조건 추가 필요
        String title = room.get(0).getTitle();
        String answer = room.get(0).getAnswer();
        String question = room.get(0).getQuestion();
        ArrayList<String> tags = new ArrayList<>();
        for(int i =0;i<room.get(0).getRoomInfos().size();i++)
        {
            tags.add(room.get(0).getRoomInfos().get(i).getTag());
        }
        RoomInfoDto roomInfo = RoomInfoDto.builder()
                .roomId(roomIdDTO.getRoomId())
                .title(title)
                .tags(tags)
                .build();
        MessageDto msg = MessageDto.builder()
                .answer(answer)
                .question(question)
                .chatMessageList(chatList)
                .codeMessageList(codeList)
                .roomInfo(roomInfo)
                .build();
        return msg;
    }

    private void postExistValidation(Optional<PostContentEntity> res){
        if(res.isPresent()){
            throw new PostAlreadyExistException();
        }
    }
}
