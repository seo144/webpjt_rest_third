package com.web.posting.service;

import com.web.posting.entity.PostInfoDTO;
import com.web.posting.entity.PostInfoEntity;
import com.web.posting.repsitories.PostInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostInfoService {
    private final PostInfoRepository postInfoRepository;

    public List<PostInfoDTO> findAllPostInfo(){
        List<PostInfoEntity> res = postInfoRepository.findAll();
        List<PostInfoDTO> ret = new ArrayList<>();
        for(int i =0;i<res.size();i++)
        {
            PostInfoDTO postInfoDTO = PostInfoDTO.builder()
                    .roomId(res.get(i).getRoomId())
                    .title(res.get(i).getTitle())
                    .build();
            List<String> tags = new ArrayList<>();
            for(int j =0;j<res.get(i).getRoomInfos().size();j++)
            {
                tags.add(res.get(i).getRoomInfos().get(j).getTag());
            }
            postInfoDTO.setTag(tags);
            ret.add(postInfoDTO);
        }
        return ret;
    }
}
