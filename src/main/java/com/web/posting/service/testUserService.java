package com.web.posting.service;

import com.web.posting.entity.testUser;
import com.web.posting.repsitories.testUserRepository;
import com.web.posting.vo.testVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@RequiredArgsConstructor
@Service
public class testUserService {
    private final testUserRepository test;

    public testVO saveInDB(testVO data)
    {
        testUser param = testUser.builder()
                        .id(data.getId())
                        .title(data.getTitle())
                        .content(data.getContent()).build();
        test.save(param);
        return data;
    }

    public testVO getItemById(String id)
    {
        testUser data = test.findById(id).get();
        return testVO.builder()
                .id(data.getId())
                .title(data.getTitle())
                .content(data.getContent())
                .build();
    }

}
