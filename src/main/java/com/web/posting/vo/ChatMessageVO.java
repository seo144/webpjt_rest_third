package com.web.posting.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatMessageVO {
    private String content;
    private Boolean exit;
    private Boolean join;
    private String joinRoom;
    private String msgDate;
    private LocalDateTime time;
    private String userName;
}
