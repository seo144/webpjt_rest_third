package com.web.posting.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CodeMessageVO {
    private String content;
    private String mark;
    private LocalDateTime time;
    private String msgDate;
    private String userName;
}
