package com.web.posting.vo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class testVO {
    private String id;
    private String title;
    private String content;
}
