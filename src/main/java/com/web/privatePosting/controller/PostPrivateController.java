package com.web.privatePosting.controller;

import com.web.privatePosting.dto.ContentSearchDto;
import com.web.privatePosting.dto.PostContentDto;
import com.web.privatePosting.dto.PostTreeDto;
import com.web.privatePosting.service.PostPrivateService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/privatePosts")
public class PostPrivateController {
    private final PostPrivateService postPrivateService;

    @PostMapping("/tree")
    public PostTreeDto createTree(@RequestBody PostTreeDto postTreeDto){
        return postPrivateService.createContentTree(postTreeDto);
    }

    @GetMapping("/cate")
    public PostTreeDto getCateg(@RequestParam UUID cateUuid){
        return postPrivateService.getCategByUuid(cateUuid);
    }

    @GetMapping("/tree")
    public List<PostTreeDto> getAllTree(){
        return postPrivateService.getAllContentsTree();
    }

    @PostMapping("/content")
    public PostContentDto createContent(@RequestBody PostContentDto postContentDto){
        return postPrivateService.createContent(postContentDto);
    }

    @GetMapping("/content-by-cate")
    public Page<PostContentDto> getContentByCate(@RequestParam UUID cateUuid, Pageable pageable){
        return postPrivateService.getContentByCate(cateUuid,pageable);
    }

    @GetMapping("/all-content-by-cate")
    public Page<PostContentDto> getAllContentByCate(@Valid ContentSearchDto contentSearchDto,Pageable pageable){
        return postPrivateService.getAllContentByCate(contentSearchDto, pageable);
    }

    @GetMapping("/content")
    public PostContentDto getContentByUuid(@RequestParam UUID contentUuid){
        return postPrivateService.getContent(contentUuid);
    }

}
