package com.web.privatePosting.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class ContentSearchDto {
    @NotNull
    private UUID cateUuid;
    private String title;
}
