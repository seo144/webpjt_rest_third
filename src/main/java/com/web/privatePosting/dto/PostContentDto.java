package com.web.privatePosting.dto;

import com.web.privatePosting.entity.PostPrivateCateEntity;
import com.web.privatePosting.entity.PostPrivateContentEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostContentDto {
    private UUID contentUuid;
    private UUID cateUuid;
    private String content;
    private String contentTitle;
    private LocalDateTime regDate;
    private LocalDateTime modDate;

    public PostPrivateContentEntity toEntity(){
        return PostPrivateContentEntity.builder()
                .contentUuid(contentUuid)
                .parent(PostPrivateCateEntity.builder().cateUuid(getCateUuid()).build())
                .content(content)
                .contentTitle(contentTitle)
                .build();
    }

    public static PostContentDto of(PostPrivateContentEntity entity){
        return PostContentDto.builder()
                .contentUuid(entity.getContentUuid())
                .cateUuid(entity.getParent().getCateUuid())
                .content(entity.getContent())
                .contentTitle(entity.getContentTitle())
                .modDate(entity.getModDate())
                .regDate(entity.getRegDate())
                .build();
    }
}
