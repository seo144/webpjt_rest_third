package com.web.privatePosting.dto;

import com.web.privatePosting.entity.PostPrivateCateEntity;
import lombok.*;

import java.util.List;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostTreeDto {
    private UUID key;
    private UUID parentUuid;
    private String label;
    private Boolean leaf;

    @Setter
    private List<PostTreeDto> children;

    public PostPrivateCateEntity toEntity(){
        return PostPrivateCateEntity.builder()
                .cateUuid(key)
                .parentUuid(parentUuid)
                .title(label)
                .isLeef(leaf)
                .build();
    }

    public static PostTreeDto of(PostPrivateCateEntity entity){
        return PostTreeDto.builder()
                .key(entity.getCateUuid())
                .parentUuid(entity.getParentUuid())
                .label(entity.getTitle())
                .leaf(entity.getIsLeef())
                .build();
    }
}
