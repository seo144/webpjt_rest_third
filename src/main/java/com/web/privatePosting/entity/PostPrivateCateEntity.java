package com.web.privatePosting.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="POST_PRIVATE_CATE")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PostPrivateCateEntity {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type="uuid-char")
    private UUID cateUuid;
    private String title;
    @Type(type="uuid-char")
    private UUID parentUuid;
    private Boolean isLeef;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    private List<PostPrivateContentEntity> contents;
}
