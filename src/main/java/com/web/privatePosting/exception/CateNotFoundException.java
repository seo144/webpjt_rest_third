package com.web.privatePosting.exception;

import com.web.common.exception.CommonException;

public class CateNotFoundException extends CommonException {
    public CateNotFoundException(){
        super(PrivatePostExceptionCode.CATE_NOT_FOUND);
    }
}
