package com.web.privatePosting.exception;

import com.web.common.exception.CommonException;

public class ContentNotFoundException extends CommonException {
    public ContentNotFoundException() {
        super(PrivatePostExceptionCode.CONTENT_NOT_FOUND);
    }
}
