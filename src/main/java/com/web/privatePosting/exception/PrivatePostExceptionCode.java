package com.web.privatePosting.exception;

import com.web.common.exception.CommonExceptionCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PrivatePostExceptionCode implements CommonExceptionCode {
    TREE_CONTENT_NOT_FOUND(500,"TREE-1","트리 정보를 찾을 수 없습니다."),
    CONTENT_NOT_FOUND(400,"CONTENT-1","Content 정보를 찾을 수 없습니다."),
    CATE_NOT_FOUND(400,"CATE-1","카테고리 정보를 찾을 수 없습니다."),
    ;

    private final int status;
    private final String errorCode;
    private final String message;

}
