package com.web.privatePosting.exception;

import com.web.common.exception.CommonException;

public class TreeContentNotFoundException extends CommonException {
    public TreeContentNotFoundException() {
        super(PrivatePostExceptionCode.TREE_CONTENT_NOT_FOUND);
    }
}
