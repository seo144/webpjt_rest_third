package com.web.privatePosting.repositories;

import com.web.privatePosting.entity.PostPrivateCateEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PostPrivateCateRepository extends JpaRepository<PostPrivateCateEntity, UUID> {
    List<PostPrivateCateEntity> findByParentUuid(UUID parentUuid);

}
