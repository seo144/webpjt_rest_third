package com.web.privatePosting.repositories;

import com.web.privatePosting.entity.PostPrivateContentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface PostPrivateContentRepository extends JpaRepository<PostPrivateContentEntity, UUID>, PostPrivateContentRepositoryCustom {
    @Query(value = "select ppce from PostPrivateContentEntity ppce join fetch ppce.parent parent where parent.cateUuid = :parentUuid order by ppce.modDate desc",
    countQuery = "select count(ppce) from PostPrivateContentEntity ppce where ppce.parent.cateUuid = :parentUuid")
    Page<PostPrivateContentEntity> findByCateUuidWithFetchPage(UUID parentUuid, Pageable pageable);
}
