package com.web.privatePosting.repositories;

import com.web.privatePosting.dto.ContentSearchDto;
import com.web.privatePosting.entity.PostPrivateContentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface PostPrivateContentRepositoryCustom {
    Page<PostPrivateContentEntity> findByCateUuidWithFetch(ContentSearchDto contentSearchDto, Pageable pageable);
}
