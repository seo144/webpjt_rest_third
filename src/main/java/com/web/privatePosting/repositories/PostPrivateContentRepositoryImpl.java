package com.web.privatePosting.repositories;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.web.privatePosting.dto.ContentSearchDto;
import com.web.privatePosting.entity.PostPrivateContentEntity;
import lombok.RequiredArgsConstructor;
import org.h2.util.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.web.privatePosting.entity.QPostPrivateCateEntity.postPrivateCateEntity;
import static com.web.privatePosting.entity.QPostPrivateContentEntity.postPrivateContentEntity;

@Repository
@RequiredArgsConstructor
public class PostPrivateContentRepositoryImpl implements PostPrivateContentRepositoryCustom {
    private final JPAQueryFactory queryFactory;


    @Override
    public Page<PostPrivateContentEntity> findByCateUuidWithFetch(ContentSearchDto contentSearchDto, Pageable pageable){
        List<PostPrivateContentEntity> query = queryFactory
                .selectFrom(postPrivateContentEntity)
                .join(postPrivateContentEntity.parent, postPrivateCateEntity)
                .fetchJoin()
                .where(
                        postPrivateCateEntity.cateUuid.eq(contentSearchDto.getCateUuid()),
                        conTitle(contentSearchDto.getTitle())
                )
                .orderBy(postPrivateContentEntity.modDate.desc())
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .fetch();

        return PageableExecutionUtils.getPage(query,pageable,findByCateUuidWithFetchCount(contentSearchDto)::fetchCount);
    }

    private JPQLQuery<PostPrivateContentEntity> findByCateUuidWithFetchCount(ContentSearchDto contentSearchDto){
        return queryFactory
                .selectFrom(postPrivateContentEntity)
                .join(postPrivateContentEntity.parent, postPrivateCateEntity)
                .where(
                        postPrivateCateEntity.cateUuid.eq(contentSearchDto.getCateUuid()),
                        conTitle(contentSearchDto.getTitle())
                ).limit(5);
    }

    public BooleanExpression conTitle(String title){
        return StringUtils.isNullOrEmpty(title)?null:postPrivateContentEntity.contentTitle.contains(title);
    }
}
