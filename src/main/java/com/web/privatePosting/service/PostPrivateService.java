package com.web.privatePosting.service;

import com.web.privatePosting.dto.ContentSearchDto;
import com.web.privatePosting.dto.PostContentDto;
import com.web.privatePosting.dto.PostTreeDto;
import com.web.privatePosting.entity.PostPrivateCateEntity;
import com.web.privatePosting.entity.PostPrivateContentEntity;
import com.web.privatePosting.exception.CateNotFoundException;
import com.web.privatePosting.exception.ContentNotFoundException;
import com.web.privatePosting.exception.TreeContentNotFoundException;
import com.web.privatePosting.repositories.PostPrivateCateRepository;
import com.web.privatePosting.repositories.PostPrivateContentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostPrivateService {
    private final PostPrivateCateRepository postPrivateCateRepository;
    private final PostPrivateContentRepository postPrivateContentRepository;

    public PostTreeDto createContentTree(PostTreeDto postTreeDto){
        PostPrivateCateEntity res = postPrivateCateRepository.save(postTreeDto.toEntity());
        return PostTreeDto.of(res);
    }

    public List<PostTreeDto> getAllContentsTree(){
        List<PostPrivateCateEntity> titles = postPrivateCateRepository.findByParentUuid(null);
        treeContentValidation(titles);
        List<PostTreeDto> ret = titles.stream().map(PostTreeDto::of).collect(Collectors.toList());
        for(PostTreeDto dto : ret){
            UUID key = dto.getKey();
            List<PostPrivateCateEntity> sub = postPrivateCateRepository.findByParentUuid(key);
            dto.setChildren(sub.stream().map(PostTreeDto::of).collect(Collectors.toList()));
        }
        return ret;
    }

    public PostContentDto createContent(PostContentDto postContentDto){
        return PostContentDto.of(postPrivateContentRepository.save(postContentDto.toEntity()));
    }

    public Page<PostContentDto> getContentByCate(UUID cateUuid, Pageable pageable){
        Page<PostPrivateContentEntity> res = postPrivateContentRepository.findByCateUuidWithFetchPage(cateUuid,pageable);
        Page<PostContentDto> ret = res.map(PostContentDto::of);
        return ret;
    }

    public Page<PostContentDto> getAllContentByCate(ContentSearchDto contentSearchDto,Pageable pageable){
        return postPrivateContentRepository.findByCateUuidWithFetch(contentSearchDto, pageable).map(PostContentDto::of);
    }

    public PostContentDto getContent(UUID contentUuid){
        return PostContentDto.of(postPrivateContentRepository.findById(contentUuid).orElseThrow(ContentNotFoundException::new));
    }

    public PostTreeDto getCategByUuid(UUID cateUuid){
        return PostTreeDto.of(postPrivateCateRepository.findById(cateUuid).orElseThrow(CateNotFoundException::new));
    }

    private void treeContentValidation(List<PostPrivateCateEntity> titles){
        if(titles.isEmpty())
            throw new TreeContentNotFoundException();
    }
}
