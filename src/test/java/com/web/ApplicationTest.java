package com.web;

import com.web.posting.entity.testUser;
import com.web.posting.repsitories.testUserRepository;
import com.web.posting.service.PostContentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

    @Autowired
    private testUserRepository testRep;

    @Autowired
    private PostContentService postContentService;

    @Test
    void test_1(){

        testUser param = testUser.builder()
                .id("test Id")
                .title("test Title")
                .content("test Content")
                .build();
        testRep.save(param);
    }
}
